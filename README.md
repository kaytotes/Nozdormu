# Nozdormu 3.3.5a Vanilla through to WotLK Timewalking Server

This Github repo is solely used as a way to keep track of Issues and Ideas related to the Nozdormu Timewalking server project. For a more general overview of what the project entails you should visit the Wiki. For bug reporting and feature requests / suggestions please visit the issue tracker. Currently this holds development information and developer submitted reports until the server opens for testing.

Code is kept in an alternate private repository.

Nozdormu is built on top of the recently released SunwellCore with additional fixes and changes tailored to the Timewalking experience.
